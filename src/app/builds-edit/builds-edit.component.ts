import { Component, Input} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Build, StorageServiceBuilds } from '../services/storage-builds.service';
import { Objeto, StorageServiceObj } from '../services/storage-obj.service';
import { Personaje, StorageService } from '../services/storage.service';

@Component({
  selector: 'app-builds-edit',
  templateUrl: './builds-edit.component.html',
  styleUrls: ['./builds-edit.component.scss'],
})
export class BuildsEditComponent {
  @Input() id: number;
  @Input() objetos: Objeto[];
  @Input() personajes: Objeto[];

  objetosItem: Objeto[] = [];

  newItem: Build = <Build>{};

  constructor(private modalCtrl: ModalController, private storageService: StorageServiceBuilds) { }

  dismissModal(modalCtrl: ModalController) {
    this.modalCtrl.dismiss();
  }


  //UPDATE
  async updatePJ(){
    this.newItem.id = this.id;
    this.newItem.item1 = this.objetosItem[0];
    this.newItem.item2 = this.objetosItem[1];
    this.newItem.item3 = this.objetosItem[2];
    this.newItem.item4 = this.objetosItem[3];
    this.newItem.item5 = this.objetosItem[4];
    this.newItem.item6 = this.objetosItem[5];
    this.storageService.updatePJ(this.newItem)
    this.modalCtrl.dismiss();

  }

}
