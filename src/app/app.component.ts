import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.changeDarkMode();
    });
  }

  changeDarkMode(){
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefersDark.matches){
    document.body.classList.toggle('dark');
      }
     }


  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Builds",
        url   : "/home",
        icon  : "aperture-sharp"
      },
      {
        title : "Personajes",
        url   : "/personajes",
        icon  : "accessibility-sharp"
      },
      {
        title : "Objetos",
        url   : "/objetos",
        icon  : "briefcase-sharp"
      },
      {
        title : "Configuracion",
        url   : "/configuracion",
        icon  : "settings-outline"
      }
    ]
  }


}
