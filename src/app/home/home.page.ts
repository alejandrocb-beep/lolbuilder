import { Component } from '@angular/core';


//local notifications
import { Plugins, LocalNotificationSchedule } from '@capacitor/core';
import { TranslateConfigService } from '../translate-config.service';
const { LocalNotifications } = Plugins;

//Builds

import { OnInit, ViewChild } from '@angular/core';
import { StorageServiceBuilds, Build } from '../services/storage-builds.service';
import {Platform, ToastController, IonList, ModalController } from '@ionic/angular';
import { BuildsEditComponent } from '../builds-edit/builds-edit.component';
import { Personaje, StorageService } from '../services/storage.service';
import { Objeto, StorageServiceObj } from '../services/storage-obj.service';
import { Storage } from '@ionic/storage';

//Compartir via Whatssapp
import { SocialSharing } from '@ionic-native/social-sharing/';

// FOR REFERENCE YOU DONT NEED THESE COMMENT LINES AS THE INTERFACE IS IMPORTED.
// interface LocalNotificationSchedule {
//   at?: Date;
//   repeats?: boolean;
//   every?: 'year'|'month'|'two-weeks'|'week'|'day'|'hour'|'minute'|'second';
//   count?: number;
//   on?: {
//     year?: number;
//     month?: number;
//     day?: number;
//     hour?: number;
//     minute?: number;
//   }
// }
const searchbar = document.querySelector('ion-searchbar');
const items = Array.from(document.querySelector('ion-list').children);


const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

async function setNotifications(config: LocalNotificationSchedule) {
  const notifs = await LocalNotifications.schedule({
    notifications: [
      {
        title: "Importante!",
        body: "Se aproxima parche! Comprueba los nuevos datos.",
        id: 1,
        schedule: config,
        sound: null,
        attachments: null,
        actionTypeId: "",
        extra: null
      }
    ]
  });
  console.log('scheduled notifications', notifs);
}




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})



export class HomePage implements OnInit {
  darkMode: boolean = true;
  selectedLanguage:string;
  userData:any = {};

  constructor(private translateConfigService: TranslateConfigService, private storageService: StorageServiceBuilds, private plt: Platform, private toastController: ToastController, 
    private modalCtrl: ModalController, private storageServicePJS: StorageService, private storageServiceOBJ: StorageServiceObj, private storage: Storage ){
    this.plt.ready().then(() => {
      this.loadItems();
      //FUNCIÓN PARA DECLARAR EN LA BBDD UN USUARIO LOCAL PARA LAS CONFIGURACIONES
      this.storage.get('userData').then((val) => {
        if (val === null){
          this.userData.name = "local";
          this.userData.tema = "light";
          this.userData.idioma = "cat";
          this.userData.tiempoNot = "minute";
          this.storage.set('userData', this.userData);
        } else {
          //cambiamos la config para el usuario local


          //tema (no funciona)
          let systemDark = window.matchMedia("(prefers-color-scheme: light)");
          
          systemDark.addListener(this.colorTest);
          if(val.tema === "dark"){
            document.body.setAttribute('data-theme', 'dark');
          }
          else{
            document.body.setAttribute('data-theme', 'light');            
          }

          //idioma
          this.selectedLanguage = val.idioma;
          
          this.translateConfigService.setLanguage(val.idioma)
          
          
          //tiempo notificación
          const conf: LocalNotificationSchedule = {
            repeats: true,
            every: val.tiempoNot,
            on: {
              day: 0 //Miercoles === 3,  DOMINGO-SABADO , horario yankee
            }
          };
          setNotifications(conf);
        }
        //console.log(val);
      });
    });
  }
  ngOnInit() {
  }


  colorTest(systemInitiatedDark) {
    if (systemInitiatedDark.matches) {
      document.body.setAttribute('data-theme', 'dark');		
    } else {
      document.body.setAttribute('data-theme', 'light');
    }
  }
  change() {
    this.darkMode = !this.darkMode;
    document.body.classList.toggle('dark');
  }
  
  items: Build[] = [];
  personajes: Personaje[] = [];
  objetos: Objeto[] = [];

  objetosItem: Objeto[] = [];


  newItem: Build = <Build>{};

  @ViewChild('mylist', { static: false })mylist: IonList;

  //CREATE

  addPJ(){

    if (this.items === null){
      this.newItem.id = 1;
    }else {
      this.newItem.id = this.items.length+1;
    }
    this.newItem.item1 = this.objetosItem[0];
    this.newItem.item2 = this.objetosItem[1];
    this.newItem.item3 = this.objetosItem[2];
    this.newItem.item4 = this.objetosItem[3];
    this.newItem.item5 = this.objetosItem[4];
    this.newItem.item6 = this.objetosItem[5];

    this.storageService.addPJ(this.newItem).then(pj =>{
      this.newItem = <Build>{};
      this.showToast('Añadida Build!');
      this.loadItems();
    })

  }

  //READ BUILDS Y PJS Y ITEMS
  loadItems(){
    this.storageService.getPJ().then(builds => {
      this.items = builds;
    });
  }

  loadItemsPJS(){
    this.storageServicePJS.getPJ().then(pjs => {
      this.personajes = pjs;
    });
  }

  loadItemsOBJ(){
    this.storageServiceOBJ.getPJ().then(obj => {
      this.objetos = obj;
    });
  }


  //UPDATE
  async updatePJ(pj: Build){
    this.loadItemsPJS();
    this.loadItemsOBJ();
    const modal = await this.modalCtrl.create({
      component: BuildsEditComponent,
      componentProps: {id: pj.id, objetos: this.objetos, personajes: this.personajes}
    });
    await modal.present();


    await modal.onDidDismiss().then(pj =>{
      this.showToast('Build Editada!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
    
  }

  //DELETE
  deletePJ(pj: Build){
    this.storageService.deletePJ(pj.id).then(pj =>{
      this.showToast('Build Eliminada!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
  }

  //HELPER

  async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  ishidden = false;

  showAdd(){
    if (this.ishidden){
      this.loadItemsPJS();
      this.loadItemsOBJ();
      this.ishidden = false;
    } else {
      this.loadItemsPJS();
      this.loadItemsOBJ();
      this.ishidden = true;
    }
    
  }


  

}
