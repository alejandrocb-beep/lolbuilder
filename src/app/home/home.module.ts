import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { TranslateModule } from '@ngx-translate/core';

import { BuildsEditComponent } from '../builds-edit/builds-edit.component';
import { HomePageRoutingModule } from './home-routing.module';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forChild(),
    HomePageRoutingModule
  ],
  declarations: [HomePage, BuildsEditComponent],
  entryComponents: [BuildsEditComponent]
})
export class HomePageModule {}
