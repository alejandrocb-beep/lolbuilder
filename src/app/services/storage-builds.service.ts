import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Objeto } from './storage-obj.service';
import { Personaje } from './storage.service';

export interface Build {
  id: number,
  personaje: Personaje,
  item1: Objeto,
  item2: Objeto,
  item3: Objeto,
  item4: Objeto,
  item5: Objeto,
  item6: Objeto
}

const ITEMS_KEY = 'builds';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceBuilds {

  constructor(private storage: Storage) { }

  //Create
  addPJ(item: Build): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Build[]) => {
      if (items){
        items.push(item);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }
  //Read
  getPJ(): Promise<Build[]>{
    return this.storage.get(ITEMS_KEY);
  }

  //Update
  updatePJ(item: Build): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((items: Build[]) => {
      if (!items || items.length === 0){
        return null;
      }

      let nuevosBds: Build[] = [];

      for (let i of items){
        if (i.id === item.id){
          nuevosBds.push(item);
        } else {
          nuevosBds.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, nuevosBds);

    });
  }

  //Delete
  deletePJ(id: number): Promise<Build>{

    return this.storage.get(ITEMS_KEY).then((items: Build[]) => {
      if (!items || items.length === 0){
        return null;
      }

      let noBorrar: Build[] = []

      for (let i of items){
        if (i.id != id){
          noBorrar.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, noBorrar);
    });
  }

}
