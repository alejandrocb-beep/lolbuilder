import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

export interface Personaje {
  id: number,
  nombre: string,
  rol: string,
  rutaIc: string
}

const ITEMS_KEY = 'personajes';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }
  //Create

  
  addPJ(item: Personaje): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Personaje[]) => {
      if (items){
        items.push(item);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }
  //Read
  getPJ(): Promise<Personaje[]>{
    return this.storage.get(ITEMS_KEY);
  }
  //Update
  updatePJ(pj: Personaje): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((pjs: Personaje[]) => {
      if (!pjs || pjs.length === 0){
        return null;
      }

      let nuevosPjs: Personaje[] = [];

      for (let i of pjs){
        if (i.id === pj.id){
          nuevosPjs.push(pj);
        } else {
          nuevosPjs.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, nuevosPjs);

    });
  }
  //Delete
  deletePJ(id: number): Promise<Personaje>{

    return this.storage.get(ITEMS_KEY).then((pjs: Personaje[]) => {
      if (!pjs || pjs.length === 0){
        return null;
      }

      let noBorrar: Personaje[] = []

      for (let i of pjs){
        if (i.id != id){
          noBorrar.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, noBorrar);
    });
  }

}
