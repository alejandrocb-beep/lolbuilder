import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

export interface Objeto {
  id: number,
  nombre: string,
  dmgAD: number,
  dmgAP: number,
  defAD: number,
  defAP: number,
  haste: number,
  itRuta: string
}

const ITEMS_KEY = 'Objetos';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceObj {

  constructor(private storage: Storage) { }
  //Create
  addPJ(item: Objeto): Promise<any> {
    return this.storage.get(ITEMS_KEY).then((items: Objeto[]) => {
      if (items){
        items.push(item);
        return this.storage.set(ITEMS_KEY, items);
      } else {
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }
  //Read
  getPJ(): Promise<Objeto[]>{
    return this.storage.get(ITEMS_KEY);
  }
  //Update
  updatePJ(pj: Objeto): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((pjs: Objeto[]) => {
      if (!pjs || pjs.length === 0){
        return null;
      }

      let nuevosPjs: Objeto[] = [];

      for (let i of pjs){
        if (i.id === pj.id){
          nuevosPjs.push(pj);
        } else {
          nuevosPjs.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, nuevosPjs);

    });
  }
  //Delete
  deletePJ(id: number): Promise<Objeto>{

    return this.storage.get(ITEMS_KEY).then((pjs: Objeto[]) => {
      if (!pjs || pjs.length === 0){
        return null;
      }

      let noBorrar: Objeto[] = []

      for (let i of pjs){
        if (i.id != id){
          noBorrar.push(i);
        }
      }
      return this.storage.set(ITEMS_KEY, noBorrar);
    });
  }

}
