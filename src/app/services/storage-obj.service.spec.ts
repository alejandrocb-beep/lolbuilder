import { TestBed } from '@angular/core/testing';

import { StorageServiceObj } from './storage-obj.service';

describe('StorageServiceObj', () => {
  let service: StorageServiceObj;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StorageServiceObj);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
