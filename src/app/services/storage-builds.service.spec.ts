import { TestBed } from '@angular/core/testing';

import { StorageServiceBuilds } from './storage-builds.service';

describe('StorageServiceBuilds', () => {
  let service: StorageServiceBuilds;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StorageServiceBuilds);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
