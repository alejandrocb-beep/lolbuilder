import { Component, Input} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Personaje, StorageService } from '../services/storage.service';

@Component({
  selector: 'app-personajes-edit',
  templateUrl: './personajes-edit.component.html',
  styleUrls: ['./personajes-edit.component.scss'],
})
export class PersonajesEditComponent {
  @Input() id: number;
  newItem: Personaje = <Personaje>{};
  constructor(private modalCtrl: ModalController, private storageService: StorageService) { }

  dismissModal(modalCtrl: ModalController) {
    this.modalCtrl.dismiss();
  }
  
  //UPDATE
  async updatePJ(){
  this.newItem.id = this.id;
  this.storageService.updatePJ(this.newItem)
  this.modalCtrl.dismiss();

  }

}
