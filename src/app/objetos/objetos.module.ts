import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ObjetosPageRoutingModule } from './objetos-routing.module';

import { ObjetosPage } from './objetos.page';

import { TranslateModule } from '@ngx-translate/core';
import { ObjetosEditComponent } from '../objetos-edit/objetos-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    ObjetosPageRoutingModule
  ],
  declarations: [ObjetosPage, ObjetosEditComponent]
})
export class ObjetosPageModule {}
