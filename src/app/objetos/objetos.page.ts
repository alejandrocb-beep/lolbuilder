import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageServiceObj, Objeto } from '../services/storage-obj.service';
import {Platform, ToastController, IonList, ModalController } from '@ionic/angular';
import { ObjetosEditComponent } from '../objetos-edit/objetos-edit.component';

@Component({
  selector: 'app-objetos',
  templateUrl: './objetos.page.html',
  styleUrls: ['./objetos.page.scss'],
})
export class ObjetosPage implements OnInit {

  constructor(private storageService: StorageServiceObj, private plt: Platform, private toastController: ToastController, 
    private modalCtrl: ModalController) {
    this.plt.ready().then(() => {
      this.loadItems();
    });
   }

  ngOnInit() {
  }

  items: Objeto[] = [];

  newItem: Objeto = <Objeto>{};

  @ViewChild('mylist', { static: false })mylist: IonList;

  //CREATE

  addPJ(){
    
    if (this.items === null){
      this.newItem.id = 1;
    }else {
      this.newItem.id = this.items.length+1;
    }
    this.newItem.itRuta = './assets/itemIcons/item'+this.newItem.id+'.png';
    this.storageService.addPJ(this.newItem).then(pj =>{
      this.newItem = <Objeto>{};
      this.showToast('Añadido Objeto!');
      this.loadItems();
    })

  }

  //READ

  loadItems(){
    this.storageService.getPJ().then(objetos => {
      this.items = objetos;
    });
  }

  //UPDATE
  async updatePJ(pj: Objeto){

    const modal = await this.modalCtrl.create({
      component: ObjetosEditComponent,
      componentProps: {id: pj.id}
    });
    await modal.present();


    await modal.onDidDismiss().then(pj =>{
      this.showToast('Objeto Editado!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
    
  }

  //DELETE
  deletePJ(pj: Objeto){
    this.storageService.deletePJ(pj.id).then(pj =>{
      this.showToast('Objeto Eliminado!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
  }

  //HELPER

  async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ishidden = false;

  showAdd(){
    if (this.ishidden){
      this.ishidden = false;
    } else {
      this.ishidden = true;
    }
    
  }


}
