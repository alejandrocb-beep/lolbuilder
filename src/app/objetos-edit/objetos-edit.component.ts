import { Component, Input} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Objeto, StorageServiceObj } from '../services/storage-obj.service';

@Component({
  selector: 'app-objetos-edit',
  templateUrl: './objetos-edit.component.html',
  styleUrls: ['./objetos-edit.component.scss'],
})
export class ObjetosEditComponent {
  @Input() id: number;
  inputValue: string = "";
  newItem: Objeto = <Objeto>{};
  constructor(private modalCtrl: ModalController, private storageService: StorageServiceObj) { }

  dismissModal(modalCtrl: ModalController) {
    this.modalCtrl.dismiss();
  }
  

  //UPDATE
  async updatePJ(){
  this.newItem.id = this.id;
  //this.newItem.nombre = "TestUpdate";
  //this.newItem.dmgAD = 10;
  //this.newItem.dmgAP = 0;
  //this.newItem.defAD = 0;
  //this.newItem.defAP = 10;
  //this.newItem.haste = 30;
  this.storageService.updatePJ(this.newItem)
  this.modalCtrl.dismiss();

  }

}
