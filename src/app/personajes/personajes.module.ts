import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PersonajesPageRoutingModule } from './personajes-routing.module';

import { PersonajesPage } from './personajes.page';
import { PersonajesEditComponent } from '../personajes-edit/personajes-edit.component';


import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule.forChild(),
    PersonajesPageRoutingModule
  ],
  declarations: [PersonajesPage, PersonajesEditComponent],
  entryComponents: [PersonajesEditComponent]
})
export class PersonajesPageModule {}
