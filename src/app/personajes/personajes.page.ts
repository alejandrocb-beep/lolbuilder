import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService, Personaje } from '../services/storage.service';
import {Platform, ToastController, IonList, ModalController } from '@ionic/angular';
import { PersonajesEditComponent } from '../personajes-edit/personajes-edit.component';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.page.html',
  styleUrls: ['./personajes.page.scss'],
})
export class PersonajesPage implements OnInit {

  constructor(private storageService: StorageService, private plt: Platform, private toastController: ToastController, 
    private modalCtrl: ModalController) {
    this.plt.ready().then(() => {
      this.loadItems();
    });
   }

  ngOnInit() {
  }

  items: Personaje[] = [];

  newItem: Personaje = <Personaje>{};

  @ViewChild('mylist', { static: false })mylist: IonList;

  //CREATE

  addPJ(){
    
    if (this.items === null){
      this.newItem.id = 1;
    }else {
      this.newItem.id = this.items.length+1;
    }
    this.newItem.rutaIc = './assets/pjsIcons/pj'+this.newItem.id+'.png';
    this.storageService.addPJ(this.newItem).then(pj =>{
      this.newItem = <Personaje>{};
      this.showToast('Añadido personaje!');
      this.loadItems();
    })

  }

  //READ

  loadItems(){
    this.storageService.getPJ().then(personajes => {
      this.items = personajes;
    });
  }

  //UPDATE
  async updatePJ(pj: Personaje){

    const modal = await this.modalCtrl.create({
      component: PersonajesEditComponent,
      componentProps: {id: pj.id}
    });
    await modal.present();


    await modal.onDidDismiss().then(pj =>{
      this.showToast('Persoaje Editado!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
    
  }

  //DELETE
  deletePJ(pj: Personaje){
    this.storageService.deletePJ(pj.id).then(pj =>{
      this.showToast('Persoaje Eliminado!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
  }

  //HELPER

  async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  ishidden = false;

  showAdd(){
    if (this.ishidden){
      this.ishidden = false;
    } else {
      this.ishidden = true;
    }
    
  }


}
